@extends('layouts.master')

@section('title', ': Admin Dashboard')

@section('css')
<!-- Dashboard Mobile Table CSS -->
<link href="{{ asset('css/dashboard-table.css') }}" rel="stylesheet" type="text/css" />
@append
@section('body')
<div class="header">
    <nav class="navbar navbar-toggleable-sm navbar-light" style="margin-bottom:10px;background-color: green;">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="#">
            <img src="{{ asset('imgs/LOGO.png') }}" style="height: 24px;">
        </a>

        <div class="collapse navbar-collapse justify-content-end" id="navbarTogglerDemo02">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link my-link" href="{{ url('/') }}">HOME</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link my-link" href="{{ url('/admin/logout') }}">LOGOUT</a>
                </li>
            </ul>
        </div>
    </nav>
</div>

<div class="container-fluid" style="height: 100%;">
    <div class="row align-items-center" style="">
        <div class="col-sm-12 col-md-10 offset-md-1">
            <h2 class="text-center" style="width:100%;">ADMIN DASHBOARD</h2>
            <br />
            <br />
            @if ($demoRequests->isNotEmpty())
                <h4 class="text-center" style="width:100%;">Request For Demo Details</h4>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>Country</th>
                            <th>Subject</th>
                            <th>Message</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $counter = 1;
                        @endphp
                        @foreach ($demoRequests as $demoRequest)
                            <tr>
                                <th>{{ $counter }}</th>
                                <td class="tdData">{{ $demoRequest->first_name }}</td>
                                <td class="tdData">{{ $demoRequest->last_name }}</td>
                                <td class="tdData">{{ $demoRequest->email }}</td>
                                <td class="tdData">{{ $demoRequest->mobile }}</td>
                                <td class="tdData">{{ $demoRequest->country }}</td>
                                <td class="tdData">{{ $demoRequest->subject }}</td>
                                <td class="tdData">{{ $demoRequest->message }}</td>
                            </tr>
                            @php
                                $counter++;
                            @endphp
                        @endforeach
                    </tbody>
                </table>
            @else
                <p class="text-center">It seems that there are no request for demo right now, please check again later</p>
            @endif
        </div>
    </div>
</div>
@endsection
