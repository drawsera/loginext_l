@extends('layouts.master')

@section('title', ': Welcome')

@section('body')
<div class="container-fluid div-full-image sections1">
    <nav class="navbar navbar-toggleable-sm navbar-light" style="margin-bottom:10px;">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="#">
            <img src="{{ asset('imgs/LOGO.png') }}" style="height: 24px;">
        </a>

        <div class="collapse navbar-collapse justify-content-end" id="navbarTogglerDemo02">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link my-link" href="#" data-toggle="modal" data-target="#myDemoModal">DEMO</a>
                </li>
                @if (auth()->check())
                <li class="nav-item">
                    <a class="nav-link my-link" href="/admin/dashboard">DASHBOARD</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link my-link" href="/admin/logout">LOGOUT</a>
                </li>
                @else
                <li class="nav-item">
                    <a class="nav-link my-link" href="#" data-toggle="modal" data-target="#myLoginModal">LOGIN</a>
                </li>
                @endif
            </ul>
        </div>
    </nav>
    <div class="container" style="height: 100%;">
        <div class="row align-items-center main-row" style="height: 90%;">
            <div class="col-sm-12 col-md-8">
                <img src="{{ asset('imgs/flag.png') }}">
                <br />
                <br />
                <h1 style="color: white; font-size: 40px; font-weight: bold;">LOREM IPSUM SAMPLE DUMMY TEXT HERE IN ONE LINE.</h1>
                <br />
                <h5 style="color: white;">Lorem ipsum dolor sit amet, epicuri vituperata vim ad. Usu autem novum sonet id, in fuisset gubergren pri, qui verear equidem an. Pri virtute mentitum in. Est et graeco ceteros invenire.</h5>
                <br />
                <br />
                <button type="button" class="trans-button" style="font-size: 18px;padding: 20px 60px" data-toggle="modal" data-target="#myDemoModal">TRY DEMO</button>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid sections2" style="background-color: white;">
    <div class="row align-items-center" style="height: auto;">
        <div class="col-lg-5 offset-lg-1 hidden-md-down">
            <img src="{{ asset('imgs/i-pad2.png') }}" style="width: 100%;">
        </div>
        <div class="col-xs-10 offset-xs-1 col-lg-4">
            <h1>LOREM IPSUM SAMPLE DUMMY TEXT HERE IN ONE LINE.</h1>
            <br />
            <br />
            <br />
            <h5>Lorem ipsum dolor sit amet, epicuri vituperata vim ad. Usu autem novum sonet id, in fuisset gubergren pri, qui verear equidem an. Pri virtute mentitum in. Est et graeco ceteros invenire.</h5>
            <br />
            <br />
            <ul style="padding-left: 19px;">
                <li>Lorem ipsum sample text here in one line</li>
                <li>Lorem ipsum sample text here in second line</li>
                <li>Lorem ipsum sample text here in one line</li>
                <li>Lorem ipsum sample text here in second line</li>
            </ul>
            <br />
            <br />
            <button type="button" class="trans-button2" style="font-size: 18px;padding: 20px 60px" data-toggle="modal" data-target="#myDemoModal">TRY DEMO</button>
        </div>

    </div>
</div>

<div style="background-color: rgb(252,252,252);">
    <div class="container sections3">
        <br />
        <div class="row align-items-center" style="height: auto;">
            <div class="col-sm-10 offset-sm-1 text-center">
                <h1 style="color: rgb(66,15,83);">LOREM IPSUM SAMPLE DUMMY TEXT HERE</h1>
                <br />
                <h5>Timeam integre ei mea, id vel quas autem aeque, vero graeco accusata eos et. At aperiri fierent disputationi sea, illud decore ex cum, eu probatus quaestio vulputate sit. Ei his mollis melius. Lorem nihil accusata usu no, ut assum prompta scripserit vel. Illud everti et cum.</h5>
                <br />
                <br />
                <br />
            </div>
            <div class="col-sm-12 col-md-4">
                <img src="{{ asset('imgs/img1.png') }}" style="padding: 5px; width: 100%;">
            </div>
            <div class="col-sm-12 col-md-4">
                <img src="{{ asset('imgs/img2.png') }}" style="padding: 5px; width: 100%;">
            </div>
            <div class="col-sm-12 col-md-4">
                <img src="{{ asset('imgs/img3.png') }}" style="padding: 5px; width: 100%;">
            </div>
        </div>
        <br />
    </div>
</div>

<footer class="my-footer" style="background-color: rgb(93,88,95);">
    <div class="container" style="height: 100%;">
        <div class="row align-items-center" style="height: 100%;color: rgb(126,123,128);">
            <div class="col-md-12 hidden-sm-down"><label>&#169; 2016 Great Event Team</label> | <a class="footer-link">Terms &amp; Conditions</a> | <a class="footer-link">Privacy Policy</a></div>

            <div class="col-sm-12 hidden-md-up"><label>&#169; 2016 Great Event Team</label></div>
            <div class="col-sm-12 hidden-md-up"><a class="footer-link">Terms &amp; Conditions</a></div>
            <div class="col-sm-12 hidden-md-up"><a class="footer-link">Privacy Policy</a></div>
        </div>
    </div>
</footer>

<!-- DEMO Modal -->
<div class="modal fade" id="myDemoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="post" action="{{ route('new-demo') }}">
                {{ csrf_field() }}
                {{ method_field('POST') }}
                <div class="modal-header">
                    <h5 class="modal-title text-center" id="exampleModalLabel" style="width:100%;">PLEASE FILL DETAILS</h5>
                </div>

                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-6">
                                <input class="form-control" type="text" name="firstName" placeholder="First Name *" value="{{ old('firstName') }}" maxlength="50">
                            </div>
                            <div class="col-sm-6">
                                <input class="form-control" type="text" name="lastName" placeholder="Last Name *" value="{{ old('lastName') }}" maxlength="50">
                            </div>
                        </div>

                        <div class="row" style="margin-top: 10px;">
                            <div class="col-sm-6">
                                <input class="form-control" required="" type="text" name="mobile" placeholder="Contact Number *" value="{{ old('mobile') }}" maxlength="20">
                            </div>
                            <div class="col-sm-6">
                                <select class="form-control" required="" name="country" id="selCountry">
                                    <option value="India">India</option>
                                    <option value="USA">USA</option>
                                    <option value="UK">UK</option>
                                </select>
                                @if (old('country') != '')
                                    <script type="text/javascript">
                                        $('#selCountry').val('{{ old('country') }}');
                                    </script>
                                @endif
                            </div>
                        </div>

                        <div class="row" style="margin-top: 10px;">
                            <div class="col-sm-12">
                                <input class="form-control" required="" type="email" name="email" placeholder="Email Address *" value="{{ old('email') }}" maxlength="100">
                            </div>
                        </div>

                        <div class="row" style="margin-top: 10px;">
                            <div class="col-sm-12">
                                <input class="form-control" type="text" name="subject" placeholder="Subject" value="{{ old('subject') }}" maxlength="100">
                            </div>
                        </div>

                        <div class="row" style="margin-top: 10px;">
                            <div class="col-sm-12">
                                <textarea class="form-control" name="message" placeholder="Message" rows="4" style="resize: none;" maxlength="500">{{ old('message') }}</textarea>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="trans-button4" data-dismiss="modal">CANCEL</button>
                    <button type="submit" class="trans-button3">SUBMIT</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- LOGIN Modal -->
<div class="modal fade" id="myLoginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="post" action="{{ url('/admin/login') }}">
                {{ csrf_field() }}
                {{ method_field('POST') }}
                <div class="modal-header">
                    <h5 class="modal-title text-center" id="exampleModalLabel" style="width:100%;">LOGIN</h5>
                </div>

                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                <input class="form-control" required="" type="email" name="email" placeholder="Email Address *" value="{{ old('email') }}">
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-sm-12">
                                <input class="form-control" required="" type="password" name="password" placeholder="Password *">
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-sm-12">
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="remember">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Remember</span>
                                </label>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="trans-button4" data-dismiss="modal">CANCEL</button>
                    <button type="submit" class="trans-button3">SUBMIT</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
    $('.my-link').click(function() {
        $(".navbar-collapse").collapse('hide');
    });
    function fnShowLogin()
    {
        $('#myModal').modal(options);
        alert('login clicked');
    }
    function fnShowDemo()
    {
        alert('demo clicked');
    }
</script>
@append
