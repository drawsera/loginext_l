@if (count($errors) > 0)
    <script type="text/javascript">
        var errorMessage = '';
        @foreach ($errors->all() as $error)
            errorMessage = errorMessage + '{{ $error }}' + '<br/>';
        @endforeach
        swal({
          title: "Opps Something Went Wrong!",
          text: errorMessage,
          type: "error",
          html: true
        });
    </script>
@endif
