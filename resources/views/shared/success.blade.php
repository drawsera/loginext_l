@if (session('success'))
    <script type="text/javascript">
        swal({
          title: "Thank You!",
          text: '{{ session('success') }}',
          type: "success",
          html: true
        });
    </script>
@endif
