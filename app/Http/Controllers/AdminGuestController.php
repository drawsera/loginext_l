<?php

namespace LogiNext\Http\Controllers;

use Illuminate\Http\Request;

class AdminGuestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function tryLogin(Request $request)
    {
        $this->validate($request, [
            // Mandatory Fields
            'email' => 'bail|required|max:100',
            'password' => 'bail|required|max:100',

            // Optional Fields
        ]);

        $credentials = array(
                                'email' => $request->email,
                                'password' => $request->password
                            );
        $rememberMe = false;
        if($request->input('remember')) {
            $rememberMe = true;
        }
        if (\Auth::attempt($credentials, $rememberMe)) {
            // this means combination of email + password + is_active is all correct
            $employer = \Auth::user();
            return redirect()->intended('/admin/dashboard');// home page
        } else {
            // this means either email OR password OR is_active is not correct, for security purpose show same message
            // go back to the login page or wherever request came from
            return back()->withErrors([
                'message' => 'Please check your login credentials and try again'
            ]);
        }
    }
}
