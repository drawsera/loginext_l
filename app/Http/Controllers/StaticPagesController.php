<?php

namespace LogiNext\Http\Controllers;

use Illuminate\Http\Request;

class StaticPagesController extends Controller
{
    /**
     * Display landing page/view
     *
     * @return \Illuminate\Http\Response
     */
    public function landingPage()
    {
        return view('landing');
    }
}
