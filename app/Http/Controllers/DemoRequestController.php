<?php

namespace LogiNext\Http\Controllers;

use Illuminate\Http\Request;

use LogiNext\DemoRequest;

class DemoRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // this will show the list of all request to admin
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            // Mandatory Fields
            'firstName' => 'bail|required|max:50',
            'lastName' => 'bail|required|max:50',

            'mobile' => 'bail|required|max:20|unique:demo_request,mobile',
            'country' => 'bail|required|max:50',

            'email' => 'bail|required|max:100|email|unique:demo_request,email',

            // Optional Fields
            'subject' => 'bail|nullable|max:100',
            'message' => 'bail|nullable|max:1500',
        ]);

        \DB::beginTransaction();
        $demoRequest = new DemoRequest();
        $demoRequest->first_name = $request->firstName;
        $demoRequest->last_name = $request->lastName;

        $demoRequest->mobile = $request->mobile;
        $demoRequest->country = $request->country;
        $demoRequest->email = $request->email;

        $demoRequest->subject = $request->subject;
        $demoRequest->message = $request->message;
        $demoRequest->save();

        \DB::commit();

        return redirect('/')
                    ->with('success', 'We have received your request for a demo and will get back to you shortly.');

    }
}
