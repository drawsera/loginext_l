<?php

namespace LogiNext\Http\Controllers;

use Illuminate\Http\Request;

use LogiNext\DemoRequest;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Logout Admin and take him to landing page/view.
     *
     * @return \Illuminate\Http\Response
     */
    public function adminLogout()
    {
        \Auth::logout();
        return redirect('/');
    }

    /**
     * Display dashboard page/view.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
        // first fetch all the Demo Request Details
        $demoRequests = DemoRequest::all();
        return view('admin.dashboard')
                ->with(compact('demoRequests'));
    }

    /**
     * Try processing Admin Login Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function tryLogin(Request $request)
    {

    }
}
