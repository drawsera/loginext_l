<?php

namespace LogiNext;

use Illuminate\Database\Eloquent\Model;

class DemoRequest extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'demo_request';
}
