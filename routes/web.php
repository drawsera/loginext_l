<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'StaticPagesController@landingPage');

Route::post('/demo-request/create', 'DemoRequestController@create')->name('new-demo');

Route::post('/admin/login', 'AdminGuestController@tryLogin');

Route::get('/admin/dashboard', 'AdminController@dashboard')->name('admin-dashboard');
Route::get('/admin/logout', 'AdminController@adminLogout')->name('admin-logout');

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
