<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDemoRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('demo_request', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';

            // Primary Key
            $table->increments('id');

            $table->string('first_name', 50);
            $table->string('last_name', 50);

            $table->string('mobile', 20)->unique();
            $table->string('country', 50);

            $table->string('email', 100)->unique();

            $table->string('subject', 100)->nullable();
            $table->string('message', 1500)->nullable();

            // Laravel Timestamp Cols
            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('demo_request');
    }
}
